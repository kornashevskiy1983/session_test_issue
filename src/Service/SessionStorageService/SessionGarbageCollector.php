<?php

namespace Service\SessionStorageService;

/**
 * Class SessionGarbageCollector
 * @package Service\SessionStorageService
 */
class SessionGarbageCollector
{
    /**
     * @return int
     */
    static public function collect()
    {
        return session_gc();
    }
}
