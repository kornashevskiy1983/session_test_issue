<?php

namespace Service\SessionStorageService\Interfaces;

interface SessionStorageInterface
{
    const FILE_PATH = __DIR__ . '/../../../var/session/';
}
