<?php

namespace Service\SessionStorageService\Interfaces;

/**
 * Interfaces SessionHandlerInterface
 * @package Service\SessionStorageService
 */
interface SessionManagerInterface
{
    /**
     * @param string $sessionId
     * @return string|null
     */
    public function read(string $sessionId): ?string;

    /**
     * @param string $sessionId
     * @param $data
     * @return bool
     */
    public function write(string $sessionId, $data): bool;

    /**
     * @param string $sessionId
     * @return bool
     */
    public function destroy(string $sessionId): bool;
}
