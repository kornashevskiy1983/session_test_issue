<?php

namespace Service\SessionStorageService;

/**
 * There is class only to show base logic of possible DBSessionHandler
 *
 * Class AppDBSessionHandler
 * @package Service\SessionStorageService
 */
class AppDBSessionHandler implements \SessionHandlerInterface, \SessionIdInterface
{
    /** @var SessionEntityClass */
    protected $repository;
    /** @var SessionEntityManager  */
    protected $sessionEntityManager;

    /**
     * AppDBSessionHandler constructor.
     * @param $em
     */
    public function __construct(SomeEntityManagerInterface $em, SessionEntityManager $sessionEntityManager)
    {
        $this->repository = $em->getRepository('SessionEntityClass');
        $this->sessionEntityManager = $sessionEntityManager;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function create_sid()
    {
        return bin2hex(random_bytes(16));
    }

    /**
     * @return bool
     */
    public function close()
    {
        return true;
    }

    /**
     * @param string $session_id
     * @return bool
     */
    public function destroy($session_id)
    {
        // TODO: Implement destroy() method.
        return $this->repository->remove($session_id);
    }

    /**
     * @param int $maxlifetime
     * @return bool
     */
    public function gc($maxlifetime)
    {
        $sessions = $this->repository->findAll();

        foreach ($sessions as $session) {
            if ($session->getCreatedAt() + $maxlifetime < time()) {
                $this->repository->remove($session);
            }
        }

        return true;
    }

    /**
     * @param string $save_path
     * @param string $name
     * @return bool
     */
    public function open($save_path, $name)
    {
        return true;
    }

    /**
     * @param string $session_id
     * @return string|null
     */
    public function read($session_id)
    {
        $session = $this->repository->find($session_id);

        return is_null($session) ? null : $session->getContent();
    }

    /**
     * @param string $session_id
     * @param string $session_data
     * @return bool
     */
    public function write($session_id, $session_data)
    {
        return $this->sessionEntityManager->write($session_id, $session_data);
    }
}
