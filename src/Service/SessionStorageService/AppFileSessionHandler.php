<?php

namespace Service\SessionStorageService;

use Service\SessionStorageService\Interfaces\SessionStorageInterface;

/**
 * Class AppFileSessionHandler
 * @package Service\SessionStorageService
 */
class AppFileSessionHandler implements \SessionHandlerInterface, \SessionIdInterface
{
    /**
     * @return string
     * @throws \Exception
     */
    public function create_sid()
    {
        return bin2hex(random_bytes(16));
    }

    public function close()
    {
        return true;
    }

    /**
     * @param string $session_id
     * @return bool
     */
    public function destroy($session_id)
    {
        $file = SessionStorageInterface::FILE_PATH.$session_id;
        if (file_exists($file)) {
            unlink($file);
        }

        return true;
    }

    /**
     * @param int $maxlifetime
     * @return bool
     */
    public function gc($maxlifetime)
    {
        foreach (glob(SessionStorageInterface::FILE_PATH.'*') as $file) {
            if (filemtime($file) + $maxlifetime < time() && file_exists($file)) {
                unlink($file);
            }
        }

        return true;
    }

    /**
     * @param string $save_path
     * @param string $name
     * @return bool
     */
    public function open($save_path, $name)
    {
        if (!is_dir(SessionStorageInterface::FILE_PATH)) {
            mkdir(SessionStorageInterface::FILE_PATH, 0777);
        }

        return true;
    }

    /**
     * @param string $session_id
     * @return false|string|null
     */
    public function read($session_id)
    {
        $content = null;

        if (is_file(SessionStorageInterface::FILE_PATH.$session_id)) {
            $content = file_get_contents(SessionStorageInterface::FILE_PATH.$session_id);
        }

        return $content;
    }

    /**
     * @param string $session_id
     * @param string $session_data
     * @return bool
     */
    public function write($session_id, $session_data)
    {
        return file_put_contents(SessionStorageInterface::FILE_PATH.$session_id, $session_data) === false ? false : true;
    }
}
