<?php

namespace Service\SessionStorageService;

use Service\SessionStorageService\Interfaces\SessionManagerInterface;

class SessionManager implements SessionManagerInterface
{
    /** @var \SessionHandlerInterface  */
    protected $sessionHandler;

    /**
     * SessionManager constructor.
     * @param $sessionHandler
     */
    public function __construct(\SessionHandlerInterface $sessionHandler)
    {
        $this->sessionHandler = $sessionHandler;
    }

    /**
     * initialize session
     */
    public function sessionInit()
    {
        session_set_save_handler($this->sessionHandler, false);
        session_start();
    }

    /**
     * @param string $sessionId
     * @return string|null
     */
    public function read(string $sessionId): ?string
    {
        return $this->sessionHandler->read($sessionId);
    }

    /**
     * @param string $sessionId
     * @param $data
     * @return bool
     */
    public function write(string $sessionId, $data): bool
    {
        return $this->sessionHandler->write($sessionId, $data);
    }

    /**
     * @param string $sessionId
     * @return bool
     */
    public function destroy(string $sessionId): bool
    {
        return $this->sessionHandler->destroy($sessionId);
    }
}
