<?php

spl_autoload_register(function ($class) {
    $class = str_replace('\\', '/', $class);

    require_once $class.'.php';
});

$sessionManager = new \Service\SessionStorageService\SessionManager(new \Service\SessionStorageService\AppFileSessionHandler());
$sessionManager->sessionInit();
$test = $sessionManager->read(session_id());
$_SESSION['test'] = 0;
$_SESSION['some'] = 0;
